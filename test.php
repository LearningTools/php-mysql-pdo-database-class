<?php 
require ("City.Class.php");
# Objeto de la tabla 
$city = new City;
# esto interpreta "SELECT * FROM tabla WHERE Id = 6"
$city->ID = "6";
$city->find();
# esto interpreta "SELECT * FROM tabla"
//$citys = $city->all();
 ?>
 <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
 <div class="container">
 	<table class="table">
           <thead>
             <tr>
               <th>#</th>
               <th>Name</th>
               <th>Country Code</th>
               <th>District</th>
             </tr>
           </thead>
           <tbody>
             <tr>
               <td><?php echo $city->ID; ?></td>
               <td><?php echo $city->Name; ?></td>
               <td><?php echo $city->CountryCode; ?></td>
               <td><?php echo $city->District; ?></td>
             </tr>
           </tbody>
         </table>
 <div class="row">
<!-- Tabla donde cargo todos lo registro de dicha tabla -->
 	<table class="table">
 	          <thead>
 	            <tr>
 	              <th>#</th>
 	              <th>Name</th>
 	              <th>Country Code</th>
 	              <th>District</th>
 	            </tr>
 	          </thead>
<?php //foreach($citys as $key){?>
 	          <tbody>
 	            <tr>
 	              <td><?php //echo $key['ID']; ?></td>
 	              <td><?php //echo $key['Name'];  ?></td>
 	              <td><?php //echo $key['CountryCode'];  ?></td>
 	              <td><?php //echo $key['District'];  ?></td>
 	            </tr>
 	            <?php //}?>
 	          </tbody>
 	        </table> 
 </div>
 <div class="row">
   <?php
# Clase que contiene la tabla persona para hacer un INSERT 
   /*
require ('CRUD/Person.Class.php');
$person         = new Person;
$person->nombre = "Kona";
$person->edad   = "20";
$person->pais   = "Francia";
$created        = $person->Create();
# Otra forma de hacer el INSERT mandando por array al constructor
$person  = new person(array("nombre"=>"Kona","edad"=>"20","pais"=>"Francia"));
$created = person->Create();
# Ambas hacen esto en SQL "INSERT INTO persona (nombre,edad,pais) VALUES ('Kona','20','Francia')"
  */
   ?>
 </div>
 <div class="row">
<?php
/*
#Clase que contiene la tabla persona para hacer un UPDATE 
require ('CRUD/Person.Class.php');
$person = new Person;
$person->nombre = "John";
$person->edad   = "20";
$person->pais   = "Portugal";
$person->email  = "Jhon20@gamil.com";
# Nos referimos a el identificador unico para poder actaulizar
$person->ID  = "27";
# Devolvemos las filas afectadas
$saved = $person->Save(); 
# Otra forma de hacer el UPDATE mandando por array al constructor
$person = new person(array("nombre"=>"John","edad"=>"20","pais"=>"Francia","Id"=>"27"));
$saved = $person->Save();
# Ambas hacen esto en el SQL "UPDATE persona SET Name = 'John',edad = 20, pais = 'Francia' WHERE Id= 27"
*/
 ?>
 </div>
 <div class="row">
<?php 
/*
# Clase que contiene la tabla persona para hacer un DELETE
require ('CRUD/Person.Class.php');
$person = new Person;
$person->Id = "21";
$deleted = $person->Delete();
# Otra forma de hacer el DELETE mandando el indicador como parametro en la instancia
//$deleted = $person->Delete(21);
# Ambas hacen esto "DELETE FROM persona WHERE Id = 21 LIMIT 1"
*/
?>
 </div>
 </div>
